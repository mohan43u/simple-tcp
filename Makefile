SRCDIR ?= $(shell pwd)
DESTDIR ?= /usr/local

HEADER := simpletcp.h
OPTIONS := -Wall -g
FLAGS := -I$(SRCDIR)
FLAGS += $(shell pkg-config --cflags --libs glib-2.0 gthread-2.0)

DISTOBJS := distributor.c
DISTOBJS += functions.c
DISTOBJS += threadfunctions.c
DISTOBJS += $(HEADER)
DIST := distributor

EXECOBJS := executor.c
EXECOBJS += functions.c
EXECOBJS += threadfunctions.c
EXECOBJS += $(HEADER)
EXEC := executor

all: distributor executor

distributor: $(DISTOBJS)
	gcc $(OPTIONS) $(FLAGS) -o $(DIST) $(DISTOBJS)

executor: $(EXECOBJS)
	gcc $(OPTIONS) $(FLAGS) -o $(EXEC) $(EXECOBJS)

install: all
	install -D $(SRCDIR)/$(DIST) $(DESTDIR)/bin/$(DIST)
	install -D $(SRCDIR)/$(EXEC) $(DESTDIR)/bin/$(EXEC)

.PHONY: clean uninstall

clean:
	rm -fr $(SRCDIR)/$(DIST)
	rm -fr $(SRCDIR)/$(EXEC)

uninstall:
	rm -fr $(DESTDIR)/bin/$(DIST)
	rm -fr $(DESTDIR)/bin/$(EXEC)
	rmdir $(DESTDIR)/bin
	rmdir $(DESTDIR)