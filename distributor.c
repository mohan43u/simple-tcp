#include <simpletcp.h>

int main(int argc, char *argv[])
{
  int inc = 1;
  FILE *inputstream = NULL;

  if(argc >= 2)
    {
      while(inc < argc)
	{
	  inputstream = fopen(argv[inc], "r");
	  distributor(inputstream);
	  fclose(inputstream);
	  inc++;
	}
    }
  else
    distributor(stdin);

  return(0);
}
