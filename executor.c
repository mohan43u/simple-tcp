#include <simpletcp.h>

int main(int argc, char *argv[])
{
  char *distributor = NULL;

  if(argc < 2)
    {
      fprintf(stderr, "[usage] executor DISTRIBUTOR\n");
      exit(EXIT_FAILURE);
    }

  distributor = strdup(argv[1]);
  executor(distributor);
  free(distributor);

  return(0);
}
