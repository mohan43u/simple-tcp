#include <simpletcp.h>

void handle_errno(const char *prefix)
{
  perror(prefix);
  exit(EXIT_FAILURE);
}

void peerdetails(const struct sockaddr *peer, socklen_t peerlen, char **peername, char **peerservice)
{
  char name[NI_MAXHOST];
  char service[NI_MAXSERV];
  int gaierror = 0;

  gaierror = getnameinfo(peer, peerlen, name, NI_MAXHOST, service, NI_MAXSERV, 0);
  if(gaierror != 0)
    {
      fprintf(stderr, "getnameinfo(): %s\n", gai_strerror(gaierror));
      exit(EXIT_FAILURE);
    }

  *peername = strdup(name);
  *peerservice = strdup(service);
}

int sendinputline(int sockfd, const char *peername, const char *peerservice, FILE *stream)
{
  char recvbuffer[BUFFERSIZE];
  ssize_t recvbytes = 0;
  ssize_t linelength = 0;
  char *inputline = NULL;
  size_t allocsize = 0;
  char *lengthstring = NULL;
  ssize_t totalsentbytes = 0;
  ssize_t sentbytes = 0;

  memset(recvbuffer, 0, BUFFERSIZE);
  recvbytes = recv(sockfd, recvbuffer, BUFFERSIZE, 0);
  if(recvbytes == -1)
    handle_errno("recv()");
  if(strncmp(recvbuffer, "ready", (strlen("ready") - 1)) != 0)
    return(1);

  linelength = getline(&inputline, &allocsize, stream);
  if(linelength <= 0)
    return(linelength);

  asprintf(&lengthstring, "%lu", linelength);
  if(send(sockfd, lengthstring, strlen(lengthstring), 0) == -1)
    handle_errno("send()");
  free(lengthstring);

  memset(recvbuffer, 0, BUFFERSIZE);
  recvbytes = recv(sockfd, recvbuffer, BUFFERSIZE, 0);
  if(recvbytes == -1)
    handle_errno("recv()");
  if(strncmp(recvbuffer, "sendline", (strlen("sendline") - 1)) != 0)
    return(1);

  while(totalsentbytes < linelength)
    {
      sentbytes = send(sockfd, &inputline[totalsentbytes], (linelength - totalsentbytes), 0);
      if(sentbytes == -1)
	handle_errno("send()");
      totalsentbytes += sentbytes;
    }
  fprintf(stderr, "send() [%s:%s]: %s", peername, peerservice, inputline);
  fflush(stderr);

  free(inputline);
  return(linelength);
}

int recvinputline(int sockfd, const char *peername, const char *peerservice, char **commandline)
{
  char recvbuffer[BUFFERSIZE];
  ssize_t recvbytes = 0;
  ssize_t linelength = 0;
  ssize_t totalrecvbytes = 0;
  char *inputline = NULL;


  if(send(sockfd, "ready", strlen("ready"), 0) == -1)
    handle_errno("send()");

  memset(recvbuffer, 0, BUFFERSIZE);
  recvbytes = recv(sockfd, recvbuffer, BUFFERSIZE, 0);
  if(recvbytes == -1)
    handle_errno("recv()");

  linelength = strtoul(strndupa(recvbuffer, recvbytes), NULL, 10);
  if(linelength <= 0)
    return(linelength);

  inputline = (char *) calloc((linelength + 1), sizeof(char));
  if(send(sockfd, "sendline", strlen("sendline"), 0) == -1)
    handle_errno("send()");

  while(totalrecvbytes < linelength)
    {
      recvbytes = recv(sockfd, &inputline[totalrecvbytes], (linelength - totalrecvbytes), 0);
      if(recvbytes == -1)
	handle_errno("recv()");
      totalrecvbytes += recvbytes;
    }
  fprintf(stderr, "recv() [%s:%s]: %s", peername, peerservice, inputline);
  fflush(stderr);

  *commandline = inputline;
  return(linelength);
}

void distributor(FILE *stream)
{
  int sockfd = -1;
  int asockfd = -1;
  struct addrinfo hints;
  struct addrinfo *result = NULL;
  int gaierror = 0;
  struct sockaddr peer;
  socklen_t peerlen;
  char *peername = NULL;
  char *peerservice = NULL;
  int acceptconnection = 1;
  int sockoptval = 1;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  gaierror = getaddrinfo(NULL, SERVICE, &hints, &result);
  if(gaierror != 0)
    {
      fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(gaierror));
      exit(EXIT_FAILURE);
    }

  sockfd = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
  if(sockfd == -1)
    handle_errno("socket()");

  if(setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockoptval, sizeof(sockoptval)) == -1)
    handle_errno("setsockopt()");

  if(bind(sockfd, result->ai_addr, result->ai_addrlen) == -1)
    handle_errno("bind()");

  if(listen(sockfd, BACKLOG) == -1)
    handle_errno("listen()");

  while(acceptconnection)
    {
      memset(&peer, 0, sizeof(struct sockaddr));
      peerlen = sizeof(struct sockaddr);
      asockfd = accept(sockfd, &peer, &peerlen);
      if(asockfd == -1)
	handle_errno("accept()");
      peerdetails(&peer, peerlen, &peername, &peerservice);
      if(sendinputline(asockfd, peername, peerservice, stream) <= 0)
	acceptconnection = 0;
      free(peername);
      free(peerservice);

      if(close(asockfd) == -1)
	handle_errno("close()");
    }

  if(close(sockfd) == -1)
    handle_errno("close()");
}

void executor(const char *distributor)
{
  int sockfd = -1;
  struct addrinfo hints;
  struct addrinfo *result = NULL;
  int gaierror = 0;
  char *peername = NULL;
  char *peerservice = NULL;
  int connection = 1;
  char *commandline = NULL;
  GThreadPool *pool = NULL;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  gaierror = getaddrinfo(distributor, SERVICE, &hints, &result);
  if(gaierror != 0)
    {
      fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(gaierror));
      exit(EXIT_FAILURE);
    }

  pool = createpool();

  while(connection)
    {
      sockfd = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
      if(sockfd == -1)
	handle_errno("socket()");

      if(connect(sockfd, result->ai_addr, result->ai_addrlen) == -1)
	{
	  if(close(sockfd) == -1)
	    handle_errno("close()");
	  break;
	}

      peerdetails(result->ai_addr, result->ai_addrlen, &peername, &peerservice);
      if(recvinputline(sockfd, peername, peerservice, &commandline) <= 0)
	connection = 0;
      else
	{
	  pushtopool(pool, commandline);
	  free(commandline);
	}
      free(peername);
      free(peerservice);

      if(close(sockfd) == -1)
	handle_errno("close()");
    }

  freepool(pool);
  freeaddrinfo(result);
}
