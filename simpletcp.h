#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <glib.h>

#define BUFFERSIZE 512
#define SERVICE "7654"
#define BACKLOG 10
#define MAXTHREADS -1

void handle_errno(const char *prefix);
void peerdetails(const struct sockaddr *peer, socklen_t peerlen, char **peername, char **peerservice);
int sendinputline(int sockfd, const char *peername, const char *peerservice, FILE *stream);
int recvinputline(int sockfd, const char *peername, const char *peerservice, char **inputline);
void distributor(FILE *stream);
void executor(const char *distributor);

void handle_gerror(GError *error, const char *prefix);
void execute(gpointer data, gpointer user_data);
GThreadPool* createpool(void);
void pushtopool(GThreadPool *pool, const char *commandline);
void freepool(GThreadPool *pool);
