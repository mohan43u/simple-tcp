#include <simpletcp.h>

void handle_gerror(GError *error, const char *prefix)
{
  fprintf(stderr, "%s: %s\n", prefix, error->message);
  exit(EXIT_FAILURE);
}

void execute(gpointer data, gpointer user_data)
{
  char *commandline = (char *) data;
  fprintf(stderr, "g_thread_self() [%p]: %s", g_thread_self(), commandline);
  fflush(stderr);
  if(system(commandline) == -1)
    handle_errno("system()");
  free(commandline);
}

GThreadPool* createpool(void)
{
  GError *error = NULL;
  GThreadPool *pool = NULL;

  g_thread_init(NULL);
  pool = g_thread_pool_new(execute, NULL, MAXTHREADS, FALSE, &error);
  if(error)
    handle_gerror(error, "g_thread_pool_new()");

  return(pool);
}

void pushtopool(GThreadPool *pool, const char *commandline)
{
  GError *error = NULL;

  g_thread_pool_push(pool, (gpointer) strdup(commandline), &error);
  if(error)
    handle_gerror(error, "g_thread_pool_push()");
}

void freepool(GThreadPool *pool)
{
  g_thread_pool_free(pool, FALSE, TRUE);
}
